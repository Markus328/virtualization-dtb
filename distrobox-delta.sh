#!/usr/bin/env bash
list_dirs=("bin" "boot" "etc" "lib" "lib64" "opt" "root" "sbin" "srv" "usr" "var")
system_dirs=("proc" "sys" "dev" "run" "tmp")
dtb_delta="/var/lib/distrobox-delta"  #### need real file system, not overlayfs
dummy_image_name="dummy-image-distrobox"
dummy_image_dirs=()
dummy_image_files=()
rootless_container=true

function get_user_subids(){
	for i in /etc/subuid /etc/subgid; do
		matching=("$(grep -E "^$USER:" $i)")
		if [[ "$matching" ]]; then
			echo $matching | cut -d':' -f2
		fi
	done
}

function choose_dummy_image_dirs(){
	for i in ${list_dirs[@]}; do
    ## Copy any symlinks (like /bin in some distros) as symlinks instead of overlay mountpoint.
		if [[ -d "/$i" && ! -L "/$i" ]]; then
			dummy_image_dirs+=("$i")
		else
      ## Symlinks and non-directories will be copied to image
			dummy_image_files+=("$i")
		fi
	done
}

function overlay_create_and_mount(){
    operation_dir="$dtb_delta/$box_name"
    sudo mkdir -p $operation_dir
	      for i in ${dummy_image_dirs[@]}; do
         sudo mkdir -p $operation_dir/.overlay/work-$i
         sudo mkdir -p $operation_dir/.overlay/upper-$i
         sudo mkdir -p $operation_dir/.container/$i
         sudo mkdir -p $operation_dir/$i
         if ! mountpoint -q $operation_dir/$i > /dev/null; then
            sudo mount  -t overlay overlay -o lowerdir=/$i,upperdir=$operation_dir/.overlay/upper-$i,workdir=$operation_dir/.overlay/work-$i $operation_dir/$i
            if $rootless_container; then
                read -d"\n" -a offsets <<<"$(get_user_subids)"
                sudo bindfs -o suid --uid-offset=${offsets[0]} --gid-offset=${offsets[1]} $operation_dir/$i $operation_dir/.container/$i
            fi
         fi
        done
}

function call_podman(){
	if $rootless_container; then
		podman "$@"
	else
		sudo podman "$@"
	fi
}

function dummy_image_create(){
if ! call_podman image exists $dummy_image_name:1 ; then
escape_dir=$(mktemp -d -p /tmp)
tarball_dir=$(mktemp -d -p /tmp)
    for i in ${system_dirs[@]} ; do
        mkdir $escape_dir/$i
    done
    for i in ${dummy_image_dirs[@]} ; do
       mkdir $escape_dir/$i
    done
		for i in ${dummy_image_files[@]}; do
			sudo cp -Ppf /$i $escape_dir/$i
		done
   cd $escape_dir
   tar -cf $tarball_dir/$dummy_image_name.tar *
    call_podman image import $tarball_dir/$dummy_image_name.tar --message tag $dummy_image_name:1
fi
}

function call_distrobox(){
	command=$1
	shift
	if $rootless_container; then
		distrobox $command "$@"
	else
    [[ "$command" = "create" ]] && command="$command --additional-flags --cap-add=ALL"
		distrobox $command --root "$@" 
	fi
}

function distrobox_invocation(){
  $rootless_container && list_volumes_dir=$dtb_delta/$box_name/.container || list_volumes_dir=$dtb_delta/$box_name
    for i in ${dummy_image_dirs[@]} ; do
        volume_mounts="${volume_mounts[@]} --volume $list_volumes_dir/$i:/$i"
    done
    call_distrobox create \
    $box_name \
    --pre-init-hooks "if [ ! -f /etc/vbox.provisioned ] ; then zypper al kernel-firm\\\*; zypper --non-interactive in -R virtualbox virtualbox-qt virtualbox-vnc; echo -e '#!/bin/bash\\\\n\\\\nkernel=\\\$(uname -r)\\\\nfor i in \\\$(find /usr/lib/modules/\\\$kernel/extra); do sudo insmod \\\$i 2>/dev/null; done ; echo 'press enter to detach' ; VirtualBox %U \\&' > /usr/local/bin/vbox-entrypoint ; chmod +x /usr/local/bin/vbox-entrypoint; touch /etc/vbox.provisioned; fi" \
    --init-hooks "usermod -aG vboxusers $USER" \
    --image localhost/$dummy_image_name:1 \
    ${volume_mounts[@]}

}
printf "###########################################################\n"
printf "                Distrobox Delta                               \n"
printf "###########################################################\n"
function main(){
choose_dummy_image_dirs
if ! call_podman container exists $box_name ; then
    overlay_create_and_mount
    dummy_image_create
    distrobox_invocation
    call_distrobox enter $box_name -- /usr/local/bin/vbox-entrypoint &
else
    overlay_create_and_mount
    call_distrobox enter $box_name -- /usr/local/bin/vbox-entrypoint &
fi
}

if [[ $(id -u) -eq 0 || "$SUDO_USER" ]]; then
	echo "running as root or sudo is not allowed."
	exit 1
fi

if [[ "$1" == "--root" ]]; then
	rootless_container=false
	shift
fi

if [ ! -z $1 ] ; then
    box_name=$1
    shift
else
    box_name="opensuse-vbox-delta"
fi

if ! $rootless_container; then
cat << EOF
a rootful container will be created
You can after enter in the container by running: 'distrobox enter --root $box_name'
EOF
else
cat << EOF
a rootless container will be created
Your password will be asked by sudo and it will only be used to setup properly the container
You can after enter in the container by runnig: 'distrobox enter $box_name' as this same user ($USER)
EOF
fi

main
